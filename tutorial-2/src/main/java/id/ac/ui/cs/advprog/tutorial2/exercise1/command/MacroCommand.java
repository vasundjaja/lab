package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // TODO Complete me!
        // for (int i = 0; i < this.commands.size(); i++) {
        //     commands.get(i).execute();
        // }
        this.commands
            .stream()
            .forEach(elem -> elem.execute());
    }

    @Override
    public void undo() {
        // TODO Complete me!
        for (int i = this.commands.size() - 1; i >= 0; i--) {
            commands.get(i).undo();
        }
        // List<Command> reverseCommands = this.commands;
        // Collections.reverse(reverseCommands);
        // reverseCommands.stream()
        //                .forEach(elem -> elem.undo());
            
    }
}
