import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {

        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t" 
                           + String.valueOf(each.getCharge()) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(totalAmount()) + "\n";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoint()) 
                                + " frequent renter points";

        return result;
    }

    public String htmlStatement() {

        Iterator<Rental> iterator = rentals.iterator();
        String result = "<h1>Rental Record for <b>" + getName() + "</b></h1>\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += "<p>" + each.getMovie().getTitle() + "\t" 
                            + String.valueOf(each.getCharge()) + "</p>\n";
        }

        // Add footer lines
        result += "<p>Amount owed is <b>" + String.valueOf(totalAmount()) + "</b></p>\n";
        result += "<p>You earned <b>" + String.valueOf(getTotalFrequentRenterPoint()) 
               + " frequent renter points</b></p>";

        return result;
    }

    private double totalAmount() {
        double total = 0;

        for (Rental each : rentals) {
            total += each.getCharge();
        }
        return total;
    }

    private int getTotalFrequentRenterPoint() {
        int total = 0;

        for (Rental each : rentals) {
            total = each.getFrequentRenterPoints(total);
        }
        return total;
    }

}