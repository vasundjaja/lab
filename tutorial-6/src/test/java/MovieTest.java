import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {

    // Hint: Make the test fixture into an instance variable
    private static Movie movieOne;
    private static Movie movieTwo;
    private static Movie movieOneCopy;

    @Before
    public void setUp() {
        movieOne = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movieTwo = new Movie("The Lion King", Movie.CHILDREN);
        movieOneCopy = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movieOne.getTitle());
    }

    @Test
    public void setTitle() {
        movieOne.setTitle("Bad Black");

        assertEquals("Bad Black", movieOne.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movieOne.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movieOne.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movieOne.getPriceCode());
    }

    @Test
    public void checkEquals() {
        assertTrue(movieOne.equals(movieOneCopy) == true);
        assertFalse(movieOne.equals(movieTwo) == true);
    }

    @Test
    public void checkHashCode() {
        assertTrue(movieOne.hashCode() == movieOneCopy.hashCode());
    }
}