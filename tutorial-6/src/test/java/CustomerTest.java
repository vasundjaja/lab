import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    // Hint: Make the test fixture into an instance variable
    private static Movie movieOne;
    private static Movie movieTwo;
    private static Movie movieThree;

    private static Customer customer;
    private static Customer customer2;

    private static Rental rent;

    @Before
    public void setUp() {
        movieOne = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movieTwo = new Movie("The Lion King", Movie.CHILDREN);
        movieThree = new Movie("Black Panther", Movie.NEW_RELEASE);

        customer = new Customer("Alice");
        customer2 = new Customer("Bob");
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        rent = new Rental(movieOne, 3);
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        rent = new Rental(movieOne, 3);
        customer2.addRental(rent);

        rent = new Rental(movieTwo, 1);
        customer2.addRental(rent);

        rent = new Rental(movieThree, 2);
        customer2.addRental(rent);

        String result = customer2.statement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 11"));
        assertTrue(result.contains("4 frequent renter points"));

    }

    @Test
    public void htmlStatement() {
        rent = new Rental(movieOne, 3);
        customer.addRental(rent);
        String htmlResult = customer.htmlStatement();

        assertEquals("<h1>Rental Record for <b>Alice</b></h1>\n" +
                "<p>Who Killed Captain Alex?\t3.5</p>\n" +
                "<p>Amount owed is <b>3.5</b></p>\n" +
                "<p>You earned <b>1 frequent renter points</b></p>", htmlResult);
    }
}