package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class StartupCompany {

    public static void main(String[] args) {
        Company startupCompany = new Company();
        
        Ceo luffy = new Ceo("Luffy", 500000.00);
        startupCompany.addEmployee(luffy);
        
        Cto zorro = new Cto("Zorro", 320000.00);
        startupCompany.addEmployee(zorro);
        
        BackendProgrammer franky = new BackendProgrammer("Franky", 94000.00);
        startupCompany.addEmployee(franky);
        
        BackendProgrammer usopp = new BackendProgrammer("Usopp", 200000.00);
        startupCompany.addEmployee(usopp);
        
        FrontendProgrammer nami = new FrontendProgrammer("Nami",66000.00);
        startupCompany.addEmployee(nami);
        
        FrontendProgrammer robin = new FrontendProgrammer("Robin", 130000.00);
        startupCompany.addEmployee(robin);
        
        UiUxDesigner sanji = new UiUxDesigner("sanji", 177000.00);
        startupCompany.addEmployee(sanji);
        
        NetworkExpert brook = new NetworkExpert("Brook", 83000.00);
        startupCompany.addEmployee(brook);
        
        SecurityExpert chopper = new SecurityExpert("Chopper", 72000.00);
        startupCompany.addEmployee(chopper);
        
        System.out.println("Here is the list of all our employees :");
        for(Employees employee : startupCompany.getAllEmployees()) {
            System.out.println("Name   : " + employee.getName() + "\n" +
                               "Role   : " + employee.getRole() + "\n" +
                               "Salary : " + employee.getSalary() + "\n");
        }
        
        System.out.println("The total salary in this company is " + startupCompany.getNetSalaries());
 
    }

}
