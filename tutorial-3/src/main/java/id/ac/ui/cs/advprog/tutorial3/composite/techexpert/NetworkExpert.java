package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {

    public NetworkExpert(String name, double salary) {
        if (salary < 50000.00) {
            throw new IllegalArgumentException("NetworkExpert Salary "
                    + "must not lower than 50000.00");
        } else {
            this.name = name;
            this.salary = salary;
            this.role = "Network Expert";
        }
    }

    @Override
    public double getSalary() {
        // TODO Auto-generated method stub
        return this.salary;
    }

}
