package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Filling {
    Food food;
    
    public ChiliSauce(Food food) {
        this.food = food;
    }
    
    @Override
    public String getDescription() {
        // TODO Auto-generated method stub
        return food.getDescription() + ", adding chili sauce";
    }

    @Override
    public double cost() {
        // TODO Auto-generated method stub
        return food.cost() + 0.3;
    }

}
