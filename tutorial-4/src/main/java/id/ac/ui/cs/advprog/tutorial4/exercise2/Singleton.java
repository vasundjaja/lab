package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // What's missing in this Singleton declaration?
    private static volatile  Singleton uniqueInstance;
    
    private Singleton() {}

    public static Singleton getInstance() {
        if (uniqueInstance == null) {
            synchronized (Singleton.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new Singleton();
                }
            }
        }
        return uniqueInstance;
    }
}
