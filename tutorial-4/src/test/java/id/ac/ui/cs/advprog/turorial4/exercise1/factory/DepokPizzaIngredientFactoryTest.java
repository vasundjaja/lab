package id.ac.ui.cs.advprog.turorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {

    private Class<?> depokPizzaIngredientFactoryTest;

    @Before
    public void setUp() throws Exception {
        depokPizzaIngredientFactoryTest = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1."
                + "factory.DepokPizzaIngredientFactory");
    }

    @Test
    public void testDepokPizzaIngredientFactoryIsAIngredientFactory() {
        Class<?>[] parent = depokPizzaIngredientFactoryTest.getInterfaces();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1."
                + "factory.PizzaIngredientFactory", parent[0].getName());
    }

    @Test
    public void testDepokPizzaIngredientFactoryOverrideCreateDoughMethod() throws Exception {
        Method createDough = depokPizzaIngredientFactoryTest.getDeclaredMethod("createDough");
        int methodModifiers = createDough.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough", 
                createDough.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testDepokPizzaIngredientFactoryOverrideCreateSauceMethod() throws Exception {
        Method createSauce = depokPizzaIngredientFactoryTest.getDeclaredMethod("createSauce");
        int methodModifiers = createSauce.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce", 
                createSauce.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testDepokPizzaIngredientFactoryOverrideCreateCheeseMethod() throws Exception {
        Method createCheese = depokPizzaIngredientFactoryTest.getDeclaredMethod("createCheese");
        int methodModifiers = createCheese.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese", 
                createCheese.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testDepokPizzaIngredientFactoryOverrideCreateVeggiesMethod() throws Exception {
        Method createVeggies = depokPizzaIngredientFactoryTest.getDeclaredMethod("createVeggies");
        int methodModifiers = createVeggies.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies[]", 
                createVeggies.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testDepokPizzaIngredientFactoryOverrideCreateClamMethod() throws Exception {
        Method createClam = depokPizzaIngredientFactoryTest.getDeclaredMethod("createClam");
        int methodModifiers = createClam.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams", 
                createClam.getGenericReturnType().getTypeName());
    }
}