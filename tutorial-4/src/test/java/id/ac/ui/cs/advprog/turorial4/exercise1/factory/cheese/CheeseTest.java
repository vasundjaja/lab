package id.ac.ui.cs.advprog.turorial4.exercise1.factory.cheese;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CheddarCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.RawClams;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CheeseTest {
    private Cheese cheddar;
    private Cheese mozzarella;
    private Cheese parmesan;
    private Cheese reggiano;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    
    @Before
    public void setUp() {
        cheddar = new CheddarCheese();
        mozzarella = new MozzarellaCheese();
        parmesan = new ParmesanCheese();
        reggiano = new ReggianoCheese();
    }
    
    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }
    
    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }
    
    @Test
    public void cheddarTestToString() {
        System.out.println(cheddar);
        assertEquals("Melted Cheddar".trim(),
                outContent.toString().trim());
    }
    
    @Test
    public void mozzarellaTestToString() {
        System.out.println(mozzarella);
        assertEquals("Shredded Mozzarella".trim(),
                outContent.toString().trim());
    }
    
    @Test
    public void parmesanTestToString() {
        System.out.println(parmesan);
        assertEquals("Shredded Parmesan".trim(),
                outContent.toString().trim());
    }
    
    @Test
    public void reggianoTestToString() {
        System.out.println(reggiano);
        assertEquals("Reggiano Cheese".trim(),
                outContent.toString().trim());
    }

}
